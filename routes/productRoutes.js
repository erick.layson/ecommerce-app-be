const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");


// Route to check if product exists exists
router.post("/checkproduct", auth.verify, productControllers.checkIfProductExists);



router.post("/add", auth.verify, productControllers.createProduct);
router.get("/active", productControllers.viewActiveProducts);
router.get("/all", productControllers.viewAllProducts);
router.get("/:productId", productControllers.viewProduct);




// Route for updating a product admin only
router.put("/:productId", auth.verify, productControllers.updateProduct);



router.patch("/archive/:productId", auth.verify, productControllers.archiveProduct);

module.exports = router;
