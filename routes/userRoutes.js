const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");
//User

// Route to check if email exists
router.post("/checkemail", userController.checkEmailExists);


router.post("/register", userController.createUser);
router.post("/login", userController.login);
router.get("/viewDetails", auth.verify, userController.viewUserDetails);
router.patch("/setadmin/:userId", auth.verify, userController.appointAdmin);

module.exports = router;
